var templates = {
	list : [
		{name : "readLater", fields : ["url","title","favicon","date"], container : "#readLater_table"}
	]
};

function getTemplate(template) {
	var templateFile = '../content/templates/' + template.name + '.html';
	
	$.ajax({
		type: 'GET',
		url: templateFile,
        error: function(xhr, ajaxOptions, thrownError) {
			//some error exception
        }
	}).done(function(response) {
		var my_template = $.templates(response);
		
		$(template.container).html(my_template.render(JSON.parse(localStorage.chromesome_readLater)));
	});
		
}

$(function() {
	
	$.each(templates.list, function() {
		getTemplate(this);
	});
	
	
});