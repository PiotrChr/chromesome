var Chromesome = function(type) {
	this.type = type;
};
	Chromesome.prototype = {
		options : {
			readLaterStorage : "chromesome_readLater",
			notebookStorage : "chromesome_notebook"
		},
		_get : function() {
			var content = localStorage[this.options[this.type + 'Storage']];
			if (typeof content === "undefined" || content.length === 0 || !(JSON.parse(content) instanceof Array)) {
				content = [];
			} else {
				content = JSON.parse(content);
			}
			return content;
		},
		_set : function(jsonObject) {
			var jsonString = JSON.stringify(jsonObject);
			if(this._get()) {
				localStorage[this.options[this.type + 'Storage']]= jsonString;
				return true;
			}
		}
	}
	

// Read later logic	
var ReadLater = new Chromesome('readLater');
	ReadLater.check = function(url) {
			var list = this._get();
			var response = [false, false];
			$.each(list, function(k,v) {
				if (this.url === url) {
					response = [true, k];
				} 
			});
			return response;
		
	};
	ReadLater.add = function(itemObject) {
		if (itemObject.url !== "undefined" && itemObject.title !== "undefined") {
			if (this.check(itemObject.url)[0] === false) {
				var content = this._get();
				content.push(itemObject);
				return this._set(content);
			} else {
				return false;
			}
		} else {
			return false;
		}
	};
	ReadLater.remove = function(url) {
		var content = this._get();
		var response = false;
		var check = this.check(url);
		if (check[0]) {
			if (content.splice(check[1])) {
				this._set(content);
			} else {
				return false;
			} 
		}
	};
	ReadLater.removeAll = function() {
		var content = [];
		this._set(content);
	};
	
// Notebook logic
var Notebook = new Chromesome();
	Notebook.add = function(itemObject) {
		
	}
	
	
// Button jQuery actions
var button = function(selector) {
	this.selector = selector;
	this.setInactive = function() {
		$(this.selector).removeClass('active').addClass('inactive');
		$(this.selector).find('.delete').css({"display":"inline-block"});
	};
	this.setActive = function() {
		$(this.selector).removeClass('inactive').addClass('active');
		$(this.selector).find('.delete').css({"display":"none"});

	};
	this.blink = function(color) {
		// later
	};
	this.changeIcon = function(fontAwesomeIcon) {
		$(this.selector).find('.fa-clock-o').removeClass(function (index, css) {
			return (css.match (/(^|\s)fa-\S+/g) || []).join(' ');
		}).addClass(fontAwesomeIcon);
	};
	
};

function loadExtensionPage(path) {
	var url = chrome.extension.getURL(path);
	chrome.tabs.create({"url":path});
}

function dayOfWeekAsString(dayIndex) {
  return ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dayIndex];
}

$(function() {
	//ReadLater.removeAll();
	
	// Check if the page is in the list
	chrome.tabs.query({"active":true}, function(tabs) {
		if (ReadLater.check(tabs[0].url)[0]) {
			var readLaterButton = new button("#_readLater");
			readLaterButton.changeIcon('fa-check');
			readLaterButton.setInactive();
		}
	});
	
	
	
	// Buttons actions
	$("body").on("click","#popup .link",function() {
		if (!($(this).hasClass('inactive'))) {
			var id = $(this).attr("id");
			var readLaterButton = new button('#'+id);
			switch(id) {
				case "_readLater":
					chrome.tabs.query({"active":true}, function(_tabs) {
						chrome.tabs.captureVisibleTab(null, {format:"jpeg",quality:30}, function (image) {
							var d = new Date();
							var date = dayOfWeekAsString(d.getDay()) + ' ' + eval(d.getMonth() + 1) + '.' + d.getFullYear() + ' | ' + d.getHours() + ':' + d.getMinutes();
							var currentTab = {};
							currentTab.thumb = image;
							currentTab.date = date;
							currentTab.url = _tabs[0].url;
							currentTab.title = _tabs[0].title;
							currentTab.favIconUrl = _tabs[0].favIconUrl;
							// get date here
							if (ReadLater.add(currentTab)) {
								readLaterButton.setInactive();
								readLaterButton.changeIcon('fa-check');
							} else {
								console.log('error while adding');
							}
						});
					});
					break;
				case "_createSegment":
					// create a segment
					break;
				case "_addToNotebook":
					// add to notebook
					break;
				case "_home":
					loadExtensionPage('content/newtab.html');
					break;
				case "_settings":
					loadExtensionPage('content/options.html');
					break;
				default:
					//nothing yet
			}
		}
	});
	$("body").on("mouseenter","#_readLater span.delete",function() {
		var desc = $(this).parent().find('.description');
		$(this).parent().addClass('red');
		var val = $(this).attr('val')
		desc.html(val);
	});
	$("body").on("mouseleave","#_readLater span.delete",function() {
		var desc = $(this).parent().find('.description');
		$(this).parent().removeClass('red');
		desc.html(desc.attr('default'))
	});
	$("body").on("click","#_readLater span.delete",function() {
		chrome.tabs.query({"active":true}, function(tabs) {
			if (ReadLater.check(tabs[0].url)[0]) {
				ReadLater.remove(tabs[0].url);
				var readLaterButton = new button('#_readLater');
				readLaterButton.changeIcon('fa-check');
				readLaterButton.setActive();
			}
		});
	});
});